//
//  ViewController2.m
//  nsuserdefault
//
//  Created by Prince Prabhakar on 17/11/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import "ViewController2.h"

@interface ViewController2 ()

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)getUser:(id)sender {
    NSString *name=[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@" Your Username"
                                                    message:name
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
@end
